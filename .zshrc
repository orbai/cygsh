# If you come from bash you might have to change your $PATH.
# export PATH=$HOME/bin:/usr/local/bin:$PATH

JAVA_HOME="/cygdrive/c/Program Files/Java/jdk1.8.0_144"
export JAVA_HOME
MVN_HOME=/cygdrive/c/apache-maven-3.3.9/bin
export MVN_HOME
export PATH=/cygdrive/c/protoc:/cygdrive/c/apache-maven-3.3.9/bin:$JAVA_HOME/bin:$MVN_HOME:$PATH

# bindkeys
bindkey "${terminfo[khome]}" beginning-of-line
bindkey "${terminfo[kend]}" end-of-line

# history options
export HISTFILE="$HOME/.histfile~"
export HISTSIZE=10000
export SAVEHIST=$((HISTSIZE/2))
setopt HIST_IGNORE_ALL_DUPS
setopt HIST_EXPIRE_DUPS_FIRST
setopt HIST_SAVE_NO_DUPS
setopt HIST_VERIFY
setopt INC_APPEND_HISTORY
setopt EXTENDED_HISTORY


# completions
fpath=(~/.zsh/completion $fpath) 
autoload -Uz compinit
compinit

zstyle ':completion:*' completer _complete _correct _approximate
zstyle ':completion:*' expand prefix suffix
zstyle ':completion:*' completer _expand_alias _complete _approximate
zstyle ':completion:*' menu select
zstyle ':completion:*' file-sort name
zstyle ':completion:*' ignore-parents pwd
zstyle ':completion:*' list-colors ${(s.:.)LS_COLORS}

# quoting URLs
autoload -U url-quote-magic
zstyle ':urlglobber' url-other-schema ftp git http https magnet
zstyle ':url-quote-magic:*' url-metas '*?[]^(|)~#='
zle -N self-insert url-quote-magic

# Sources import
source ~/.zshenv/aliases
source ~/.zshenv/functions
source ~/.zshenv/prompt
source ~/.zshenv/mvn
source ~/.zshenv/sublime
